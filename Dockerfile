FROM python:3.8-slim-buster

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

RUN rm requirements.txt

COPY autoscaling-exporter.py autoscaling-exporter.py

CMD [ "python", "autoscaling-exporter.py" ]
