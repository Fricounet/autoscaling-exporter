# Autoscaling exporter

**Base image:** [`python 3.8-slim-buster`](https://hub.docker.com/_/python)

### Packages installed

- Boto 3
- prometheus-client

### Used in

- [Cluster-config](https://gitlab.laforge.cloud.bpifrance.fr/project/BRC/infrastructure/cluster-config/-/pipelines)

### Changelog

- 1.0.0 (2021-06-07): Init exporter
- 1.0.1 (2021-06-07): Clear old metrics to prevent oom
