from prometheus_client import start_http_server, Enum
import time
import boto3

session = boto3.Session(
        region_name="eu-west-3"
    )
autoscaling = session.client('autoscaling')

# Create a metric to track the state of the ASG.
ASG_STATE = Enum('aws_auto_scaling_group_state', 'The state of the autoscaling group',
                 ['asg_name', 'cause', 'description'],
                 states=['running', 'stopped', 'unknown'])

def main():
    # Store for metrics
    store = []

    # Get all autoscaling group names
    auto_scaling_groups = [auto_scaling_group['AutoScalingGroupName'] for auto_scaling_group in autoscaling.describe_auto_scaling_groups()['AutoScalingGroups']]
    for auto_scaling_group_name in auto_scaling_groups:
        last_activity = next(iter(autoscaling.describe_scaling_activities(AutoScalingGroupName=auto_scaling_group_name, MaxRecords=1)['Activities']), {})
        if last_activity:
            cause, description = last_activity['Cause'], last_activity['Description']

            if last_activity['StatusCode'] == 'Successful':
                state = 'running'
            else:
                state = 'stopped'
        else:
            cause, description = 'No data', 'No scaling activity was recorded for this autoscaling group'
            state = 'unknown'

        # Add metric to store
        store.append((auto_scaling_group_name, cause, description, state))

    # Clear existing metrics
    ASG_STATE.clear()
    for metric in store:
        auto_scaling_group_name, cause, description, state = metric
        # Update metric
        ASG_STATE.labels(auto_scaling_group_name, cause, description).state(state)

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    # Generate metrics.
    while True:
        main()
        time.sleep(10)
